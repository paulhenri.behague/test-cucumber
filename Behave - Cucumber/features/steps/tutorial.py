from behave import *
import allocation
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
import time

options = Options()
options.headless = True

@given('a total budget of "{total_budget}"')
def step_impl(context,total_budget):
	context.budget = int(total_budget)
	pass

@given('an hospital list')
def step_impl(context):
	context.hospitalList = {
	'hospital1': 300,
	'hospital2': 500,
	'hospital3': 800
	}

@given('a launched browser on main ATIH page')
def step_impl(context):
	context.driver = webdriver.Firefox(options=options)
	pass

@given('a "{user_firstname}" and a "{user_lastname}"')
def step_impl(context, user_firstname, user_lastname):
	context.firstName = user_firstname
	context.lastName = user_lastname
	pass




@when('the cost allocation matrix is generated')
def step_impl(context):
	context.allocations = allocation.allocate_cost(context.budget,
		context.hospitalList)
	pass

@when('the main page is visited and searched for user identity')
def step_impl(context):
	context.driver.get("https://www.atih.sante.fr/")
	time.sleep(1)
	elem = context.driver.find_element_by_id('edit-terms')
	elem.send_keys(context.firstName + ' ' + context.lastName)
	elem.send_keys(Keys.ENTER)
	pass




@then('the output is equal to the total budget')
def step_impl(context):
	total = 0
	for hospitalBudget in context.allocations:
		total += hospitalBudget
	assert total == context.budget
	#assert total == context.budget + 1

@then('all allocations must be superior to "{minimum_budget}"')
def step_impl(context,minimum_budget):
	minBudget = int(minimum_budget)
	assert all([a >= minBudget for a in context.allocations]) is True

@then('the searched text appears in results header')
def step_impl(context):
	block = WebDriverWait(context.driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "h1.text-title-flat")))
	texts = block.find_elements_by_css_selector("strong")
	text = texts[1].get_attribute("innerHTML")
	assert text == (context.firstName + ' ' + context.lastName)

#behave --lang-help fr
