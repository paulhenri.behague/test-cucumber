Feature: Evaluate cost allocation

  Scenario: Test all budget is allocated
  	Given a total budget of "500"
  	And an hospital list
  	When the cost allocation matrix is generated 
  	Then the output is equal to the total budget


  Scenario Outline: Test minimum allocations
  	Given a total budget of "<total_budget>"
  	And an hospital list
  	When the cost allocation matrix is generated
  	Then all allocations must be superior to "<minimum_budget>"

  Examples: Values below 1000
    | total_budget  | minimum_budget |
    | 100           | 30             |
    | 200           | 60             |
    #| 100           | 40             |

  Scenario Outline: Launch browser and search for people
    Given a launched browser on main ATIH page
    And a "<user_firstname>" and a "<user_lastname>"
    When the main page is visited and searched for user identity
    Then the searched text appears in results header
  Examples: User firstname and lastname
    | user_firstname  | user_lastname |
    | Max             | Bensadon      |
    | Emmanuel        | Thammavong    |