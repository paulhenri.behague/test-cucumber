<script type="module" src="jsonToMessage.js"></script>

import { jsonToMessages } from '@cucumber/json-to-messages'

const JSONs: Readable = behave_json.json // The JSON reports to translate
const outputStream: Writable = my_report.messages // A sstream to write the messages to
jsonToMessages(JSONs, outputStream)
